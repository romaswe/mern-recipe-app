const roles = {
	VIEWER: 'viewer',
	USER: 'user',
	ADMIN: 'admin',
	DISABLED: 'disabled',
};

module.exports = roles;
